# INF3135 - Outils de correction

Outils de correction du cours INF3135 pour la session d'hiver 2018 à
l'Université du Québec à Montréal. Le dépôt contient différents outils:

- révision de notes;
- publication automatique d'un fichier de correction au format «Markdown» sur
  Gitlab;
- publication automatique de notes pour travaux dans des plages d'une feuille de
  calcul Google Sheet.

## Améliorations à venir

- [x] Générécité du module `markfile.py` afin de rendre possible la révision et
  publication sur gitlab de résultats de volets différents. Jusqu'ici, que le
  volet 2 du TP1 est totalement supporté;

<!-- vim: set ts=4 sw=4 tw=80 et :-->

