"""""""""""""""""
"  DÉPENDENCES  "
"""""""""""""""""

" - fugitive (https://github.com/tpope/vim-fugitive)
"   - commande 'Commit'
" - vim-markdown (https://github.com/plasticboy/vim-markdown)
"   - <leader>p pour 'TableFormat'
" - perl: calcul des points (expression arithmétique)

"""""""""""""""""""
"  CONFIGURATION  "
"""""""""""""""""""

" TODO: ajouter la liste des sections dans lesquelles récupérer des pointages de
" section. Par exemple:
"
"   let s:sections = [
"         \ 'Documentation du projet',
"         \ 'Makefile',
"         \ 'Requête d'intégration']
"
let s:sections = [ ]

" TODO: configurer le message de validation automatique utilisé par la command
" :Commit. N.B: Le message doit contenir des guillemets doubles. Par exemple:
"
"   let s:commit_message = '"correction: readme, makefile, RI"
"
let s:commit_message = ''

""""""""""""""""""
"  REGEX UTILES  "
""""""""""""""""""

let s:tdb = '<td>'
let s:tde = '<\/td>'

let s:section_title_re = '^#\+\s*'

let s:td_re    = '^\s*' . s:tdb
let s:num_re   = '\([0-9]\+\(\.[0-9]\+\)\?\)\?'
let s:total_re = '^\*\*Total\*\*:\s*' . s:num_re

"""""""""""""""""""""""""""""""""""""""""""""
"  REMPLISSAGE DU TABLEAU GÉNÉRAL DE NOTES  "
"""""""""""""""""""""""""""""""""""""""""""""

fun! s:get_section_justifications(begin, end)
  call cursor(a:begin,0)
  let l:re = '^\*\*\(.*\)\s\+(' . s:num_re . ' pts)\*\*$'
  let l:j  = ''
  let l:mc = 0
  while search(l:re, 'W') != 0 && line('.') < a:end
    let l:g_str = matchlist(getline('.'), l:re)
    if len(l:g_str) >= 2
      let l:j  .= (l:mc > 0 ? ', ' : '') . l:g_str[1]
      let l:mc += 1
    endif
  endw
  return l:j
endf

fun! s:get_all_justifications()
  let l:j = []
  for s in s:sections
    call cursor(1,1)
    call search(s:section_title_re . l:s, 'W')
    let l:begin = line('.')
    let l:ret = search(s:section_title_re, 'W')
    let l:end = l:ret != 0 ? line('.') : line('$')
    let l:j += [s:get_section_justifications(l:begin, l:end)]
  endfor
  return l:j
endf

fun! s:fill_justifications()
  let l:j = s:get_all_justifications()
  call cursor(1,1)
  for i in range(len(s:sections))
    call search(s:tdb . s:sections[i] . s:tde)
    call search(s:td_re)
    " supprime le contenu dans le
    normal 0f<dit
    call setline(line('.'), substitute(getline('.'), '<td><\/td>', '<td>'.l:j[i].'</td>', ""))
  endfor
endf

fun! s:fill_marks()
  let l:mark_re  = s:tdb . '.*\/\s*' . s:num_re . s:tde
  call cursor(1,1)

  let l:c = 0
  for s in s:sections
    " get to the total cell
    call search(s:tdb . l:s . s:tde)
    call search(s:td_re)
    call search(s:td_re)
    let l:pos = getpos('.')
    " get to the total
    for s:_ in range(l:c+1)
      call search(s:total_re)
    endfor
    let l:m = matchlist(getline('.'), s:total_re)
    if len(l:m) > 1
      let l:points = l:m[1]
    endif
    " go back to total cell
    call setpos('.', l:pos)
    let l:m = matchlist(getline('.'), l:mark_re)
    if len(l:m) > 1
      let l:total = l:m[1]
    endif
    call setline(
          \ line('.'),
          \ substitute(getline('.'), l:mark_re, '<td>'.l:points.'/'. l:total .'</td>', "")
          \ )
    let l:c += 1
  endfor
endf

fun! s:fill_table()
  let l:pos = getpos('.')
  call s:fill_justifications()
  call s:fill_marks()
  call setpos('.', l:pos)
endf

""""""""""""""""""""""""""""""""""""""""""""""
"  CALCUL DE LA NOTE PAR SECTION (TABLEAU)   "
""""""""""""""""""""""""""""""""""""""""""""""

fun! s:table_total_points()
  let l:pos = getpos('.')
  let l:mark_re = '\(|.*|\)\(.*\)|'
  let l:sum     = '0'
  call search('^$', 'b')
  call cursor(line('.')+2, 0)
  call search(l:mark_re, 'W')
  while match(getline('.'), l:mark_re) > -1
    let l:m = str2float(
          \ system("perl -e '$x=".matchlist(getline('.'), l:mark_re)[2]."; print $x'")
          \ )
    call setline(line('.'), substitute(getline('.'), l:mark_re, '\1 '.string(l:m).' |', ""))
    let l:sum += l:m
    call cursor(line('.')+1, 0)
  endw
  if search(s:total_re, 'W') > 0
    call setline(
          \ line('.'),
          \ substitute(getline('.'), s:total_re, '**Total**: ' . string(l:sum), "")
          \ )
  endif
  call setpos('.', l:pos)
endf

"""""""""""""""""""""""""""
"  COMMANDES ET MAPPINGS  "
"""""""""""""""""""""""""""

command! FillTable call s:fill_table()
command! Total call s:table_total_points()
command! Commit execute "Git checkout -B correction master" | Gwrite | execute 'Gcommit -m ' . s:commit_message

nnoremap <leader>p :Total<cr>:TableFormat<cr>
nnoremap <leader>f :FillTable<cr>

" vim: set ts=2 sw=2 tw=100 et :

