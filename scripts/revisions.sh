#!/bin/bash

print_usage() {
  printf "revisions.sh -- Effectue la révision des notes\n"
  printf "\n"
  printf "SYNOPSIS\n"
  printf "  revisions.sh -h\n"
  printf "  revisions.sh [-f] [-E] [-P] -p {project} -b {issue_body_file} -t {token}\n"
  printf "               -s {section} -r {revision_spec} {dépôt} ...\n"
  printf "\n"
  printf "OPTIONS\n"
  printf "\n"
  printf "  -h|--help\n"
  printf "      Affiche le texte d'aide.\n"
  printf "  -f|--force\n"
  printf "      Exécute la révision de note même si la plus récente est d'aujourd'hui.\n"
  printf "  -E|--no-edit\n"
  printf "      Ne pas modifier la note.\n"
  printf "  -P|--no-publish\n"
  printf "      Ne pas publier les résultats.\n"
  printf "  -A|--no-prepare\n"
  printf "      Ne pas préparer le dépôt git (empêche \`git reset @ --hard\`).\n"
  printf "  -p|--project {project}\n"
  printf "      Le nom du projet.\n"
  printf "  --issue-title {title}\n"
  printf "      Le titre du billet à ouvrir.\n"
  printf "  --comment-title {title}\n"
  printf "      Le titre du commentaire.\n"
  printf "  -b|--issue-body-file {file}\n"
  printf "      Le chemin vers le fichier contenant le corps du billet à publier.\n"
  printf "  -t|--token {token}\n"
  printf "      Passe le jeton {token} afin de s'authentifier lors la connexion à Gitlab.\n"
  printf "  -s|--section {section}\n"
  printf "      La section à réviser.\n"
  printf "  -r|--revision {rev_sec}\n"
  printf "      La spécification de la révision (format JSON). Voir \`revision --help\`\n"
  printf "      pour plus d'information.\n"
}

program_name_=revisions.sh
options='hfEPAp:b:t:s:r:'
loptions='help,force,no-edit,no-publish,no-prepare,project:,issue-title:,comment-title:,issue-body-file:,token:,section:,revision:'
getopt_out=$(getopt --name $program_name_ --options $options --longoptions $loptions -- "$@")
if (( $? != 0 )); then exit 1; fi
eval set -- "$getopt_out"

force_=false
no_publish_=false
no_edit_=false
no_prepare_=false
section_=''
revision_=''
project_=''
issue_title_=''
comment_title_=''
issue_body_file_=''
token_=''
while [[ $1 != "--" ]]; do
  case "$1" in
    -h|--help)
      print_usage
      exit 0
      ;;
    -f|--force)
      force_=true
      shift
      ;;
    -E|--no-edit)
      no_edit_=true
      shift 1
      ;;
    -P|--no-publish)
      no_publish_=true
      shift 1
      ;;
    -A|--no-prepare)
      no_prepare_=true
      shift 1
      ;;
    -p|--project)
      project_="$2"
      shift 2
      ;;
    --issue-title)
      issue_title_="$2"
      shift 2
      ;;
    --comment-title)
      comment_title_="$2"
      shift 2
      ;;
    -b|--issue-body-file)
      issue_body_file_="$2"
      shift 2
      ;;
    -t|--token)
      token_="$2"
      shift 2
      ;;
    -s|--section)
      section_="$2"
      shift 2
      ;;
    -r|--revision)
      revision_="$2"
      shift 2
      ;;
  esac
done

shift

########################
#  VARIABLES D'ENTRÉE  #
########################

dirs_="$@"
revision_version_="6e1db37.$(date +%F)"

markfile_name=correction.md
revision_bin_="revision.py"
publisher_bin_="gitlabpublisher"
revision_commit_msg_="correction: révision $revision_version_"

# modificateurs de couleurs
RED="\033[0;31m"
GREEN="\033[0;32m"
NO_COLOUR="\033[0m"
BOLD="\e[1m"

###############################
#  FONCTIONS DE FACILITATION  #
###############################

# Écrire l'utilisateur révisé.
print_revision() { printf "${BOLD}Révision de \'${1}\':${NO_COLOUR} "; }

# Écrire le message de succès après révision de note.
print_success_revision() { printf "${GREEN}OUI${NO_COLOUR}!\n"; }

# Écrire l'utilisateur pour lequel on fait la publication
print_publishing() { printf "${BOLD}Publication de la révision pour \'${1}\':${NO_COLOUR}\n"; }

# Écrire le message de succès après publication.
print_success_publishing() { printf "${GREEN}OK${NO_COLOUR}!\n"; }

# Écrire le message d'échec. Celui-ci est composé du mot "NON", suivi du texte
# additionnel passé en paramètre ($1).
print_failure() { printf "${RED}NON${NO_COLOUR}.\n${1}\n"; }

# Prépare le répertoire de façon à ce qu'on parte d'une validation propre.
prepare_dir() { $no_prepare_ || git reset @ --hard >/dev/null; }

# Fait un commit dans le répertoire $1 avec le nouveau contenu du fichier $1/$2.
git_commit() {
  git add $1
  git commit -m "$2"
}

check_not_revised_today() {
  yesterday=$(date -d 'yesterday' +%F)
  revision_commits=$(git log --since=$yesterday --oneline | grep -m 1 "${revision_commit_msg_:0:20}")
  (( $? > 0 )) && return 0 || return 1
}

#########
#  JOB  #
#########

rc_=1

for dir in $dirs_; do
  user="$dir"
  markfile=${user}/${markfile_name}
  [[ -d ${dir} && -f ${markfile} ]] || continue

  ( cd $dir && { ${force_} || check_not_revised_today ; } && prepare_dir )
  if (( $? == 0 )) && ! ${no_edit_}; then
    print_revision $user
    ${revision_bin_} -r "${revision_}" -s ${section_} < ${markfile} > ${markfile}.0
    if (( $? != 0 )); then
      print_failure
      rm -f ${markfile}.0
      continue
    fi

    print_success_revision
    mv ${markfile}.0 ${markfile}
    ( cd $dir && git_commit ${markfile_name} "$revision_commit_msg_" )
  fi

  ${no_publish_} && continue

  print_publishing $user
  ${publisher_bin_} -p ${project_} -Ti "${issue_title_}" -b ${issue_body_file_} -Tc "${comment_title_}" -t $token_\
                     -u $user < ${markfile}
  if (( $? != 0 )); then
    # Échec de publication
    print_failure
  else
    print_success_publishing
    # au moins un dépôt a fonctionné
    (( $rc_ != 0 )) && rc_=0
  fi
done

exit $rc_

# vim: set ts=2 sw=2 tw=120 et :

