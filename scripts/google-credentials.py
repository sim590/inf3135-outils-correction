#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2018 Google. All Rights Reserved.

from __future__ import print_function
import os

from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

import argparse
parser = argparse.ArgumentParser(parents=[tools.argparser])
parser.add_argument("secret", help="JSON file containing OAuth2 info.", type=str)
flags = parser.parse_args()

SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
APPLICATION_NAME = 'Google Sheets API Python W/E'

def store_credentials():
    """The OAuth2 flow is completed to obtain the new credentials to store them into
    ~/.credentials/sheets.googleapis.com-python-quickstart.json.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'sheets.googleapis.com-python-quickstart.json')

    flow = client.flow_from_clientsecrets(flags.secret, SCOPES)
    flow.user_agent = APPLICATION_NAME
    credentials = tools.run_flow(flow, Storage(credential_path), flags)
    print('Storing credentials to ' + credential_path)

if __name__ == '__main__':
    store_credentials()

#  vim: set ts=8 sw=4 tw=120 et :

