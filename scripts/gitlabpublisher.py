#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import argparse
import ipdb
import json
import requests
import urllib
import traceback

import progressbar

import markfile

PRG_NAME = "gitlabpublisher"
VERSION  = 0.1

def raise_error(error, text):
    raise error("%s: Erreur: %s" % (PRG_NAME, text))

class GitlabPublisher:
    BASE_API_URL       = "https://gitlab.com/api/v4"
    PRIVATE_TOKEN_KEY  = "PRIVATE-TOKEN"
    API_SUCCESS_GET    = 200
    API_SUCCESS_PUT    = 200
    API_SUCCESS_POST   = 201
    API_SUCCESS_DELETE = 204

    def __init__(self, token):
        self._token = token
        self._auth_header = {GitlabPublisher.PRIVATE_TOKEN_KEY : self._token}

    def _enquote_username_dots(self, username):
        return username.replace('.', '%2E')

    def get_project_info(self, user, project):
        """Retrouve le projet de l'utilisateur.

        @param user:  Le nom d'utilisateur.
        @type  user:  str

        @return:  Les informations sur le projet (réponse JSON).
        @rtype :  dict

        @raise RuntimeError:  Si la réponse n'a pas un code HTTP 200.
        """
        url = "%s/users/%s/projects?search=%s" \
                % (GitlabPublisher.BASE_API_URL, self._enquote_username_dots(user), urllib.parse.quote(project))
        pr = requests.Request('GET', url, headers=self._auth_header).prepare()
        # on contourne le formatage de l'URL automatique. Ceci est dû au fait que
        # Gitlab ne prend pas les URLs avec des noms d'utilisateurs contenant
        # des points. Il faut donc forcer l'URL.
        #
        # FIXME: C'est peut-être fragile car si un jour il y avait un formatage
        # à faire à l'URL, ça planterait.
        pr.url = url
        s = requests.Session()
        r = s.send(pr)
        if r.status_code != GitlabPublisher.API_SUCCESS_GET:
            print(r.content, file=sys.stderr)
            raise RuntimeError(PRG_NAME+":"+" "+"Erreur: get_project_info, status code: %s, url: %s" % (r.status_code, r.url))
        # On suppose que la recherche est suffisamment précise. ) Sinon,
        # préciser PROJECT_SEARCH.
        return r.json()[0]

    def get_issue(self, pid, ititle):
        """Retrouve le billet sur lequel se trouve la correction.

        @param pid:  Identifiant du projet.
        @type  pid:  int

        @param ititle:  Le titre du billet.
        @type  ititle:  str

        @return:  Les informations sur le billet (réponse JSON).
        @rtype :  dict

        @raise RuntimeError:  Si la réponse n'a pas un code HTTP 200.
        """
        url = "%s/projects/%s/issues" % (GitlabPublisher.BASE_API_URL, pid)
        r = requests.get(url, headers=self._auth_header)
        if r.status_code != GitlabPublisher.API_SUCCESS_GET:
            print(r.content, file=sys.stderr)
            raise_error(RuntimeError, "get_issue_comment, status code: %s, url: %s" % (r.status_code, r.url))
        for j in r.json():
            if ititle == j['title']:
                return j
        return None

    def publish_issue(self, pid, ititle, description, iid=None):
        """Publie un billet sur gitlab.

        @param pid: L'identifiant du projet.
        @type  pid: int

        @param ititle: Le titre du billet.
        @type  ititle: str

        @param description: La description du billet.
        @type  description: str

        @param iid:  Identifiant du billet («issue» dans le jargon gitlab).
        @type  iid:  int

        @return: Les informations sur le billet (réponse JSON).
        @rtype : dict

        @raise RuntimeError:  Si la réponse n'a pas un code HTTP 201.
        """
        r     = None
        error = None
        url   = "%s/projects/%s/issues/" % (GitlabPublisher.BASE_API_URL, pid)
        data  = { 'title' : ititle, 'description' : description }

        if iid:
            url += "/%s" % iid
            r = requests.put(url, data=data, headers=self._auth_header)
            error = r.status_code != GitlabPublisher.API_SUCCESS_PUT
        else:
            r = requests.post(url, data=data, headers=self._auth_header)
            error = r.status_code != GitlabPublisher.API_SUCCESS_POST

        if error:
            print(r.content, file=sys.stderr)
            raise_error(RuntimeError, "publish, status code: %s, url: %s" % (r.status_code, r.url))

        return r.json()

    def get_issue_comment(self, pid, iid, ntitle):
        """Retrouve le commentaire sur lequel faire la publication.

        @param pid:  Identifiant du projet.
        @type  pid:  int

        @param iid:  Identifiant du billet.
        @type  iid:  int

        @param ntitle:  Le titre du commentaire.
        @type  ntitle:  str

        @return:  Les informations sur le commentaire (réponse JSON).
        @rtype :  dict

        @raise RuntimeError:  Si la réponse n'a pas un code HTTP 200.
        """
        url = "%s/projects/%s/issues/%s/notes" % (GitlabPublisher.BASE_API_URL, pid, iid)
        r = requests.get(url, headers=self._auth_header)
        if r.status_code != GitlabPublisher.API_SUCCESS_GET:
            print(r.content, file=sys.stderr)
            raise_error(RuntimeError, "get_issue_comment, status code: %s, url: %s" % (r.status_code, r.url))
        maxs = 0.0
        comment = None
        for j in r.json():
            i, k = markfile.extract_title_indexes(j['body'])
            if ntitle in j['body'][i:k]:
                return j
        return None

    def publish_comment(self, pid, iid, comment, nid=None):
        """Publie le commentaire sur la note d'identifiant nid pour le projet d'identifiant pid.

        @param pid:  Identifiant du projet.
        @type  pid:  int

        @param iid:  Identifiant du billet («issue» dans le jargon gitlab).
        @type  iid:  int

        @param comment:  Le texte du commentaire.
        @type  comment:  str

        @param nid:  Identifiant du commentaire («note» dans le jargon gitlab).
        @type  nid:  int

        @return:  Les informations sur le commentaire (réponse JSON).
        @rtype :  dict

        @raise RuntimeError:  Si la réponse n'a pas un code HTTP 200 (put) ou 201 (POST).
        """
        r     = None
        error = None
        url   = "%s/projects/%s/issues/%s/notes" % (GitlabPublisher.BASE_API_URL, pid, iid)
        data  = { 'body' : comment }

        if nid:
            url += "/%s" % nid
            r = requests.put(url, data=data, headers=self._auth_header)
            error = r.status_code != GitlabPublisher.API_SUCCESS_PUT
        else:
            r = requests.post(url, data=data, headers=self._auth_header)
            error = r.status_code != GitlabPublisher.API_SUCCESS_POST

        if error:
            print(r.content, file=sys.stderr)
            raise_error(RuntimeError, "publish_comment, status code: %s, url: %s" % (r.status_code, r.url))

        return r.json()

    def get_project_members(self, pid):
        """Retrouve les membres d'un projet.

        @param pid:  Identifiant du projet.
        @type  pid:  int

        @return:  Les informations sur les membres (réponse JSON).
        @rtype :  dict

        @raise RuntimeError:  Si la réponse n'a pas un code HTTP 200.
        """
        url   = "%s/projects/%s/members" % (GitlabPublisher.BASE_API_URL, pid)
        r     = requests.get(url, headers=self._auth_header)
        error = r.status_code != GitlabPublisher.API_SUCCESS_GET

        if error:
            print(r.content, file=sys.stderr)
            raise_error(RuntimeError, "get_project_members, status code: %s, url: %s" % (r.status_code, r.url))
        return r.json()

    def remove_user_from_project(self, pid, uid):
        """Retire l'utilisateur de la liste des membres du projet.

        @param pid:  Identifiant du projet.
        @type  pid:  int

        @param uid:  Identifiant de l'utilisateur.
        @type  uid:  int

        @raise RuntimeError:  Si la réponse n'a pas un code HTTP 204.
        """
        url   = "%s/projects/%s/members/%s" % (GitlabPublisher.BASE_API_URL, pid, uid)
        r = requests.delete(url, headers=self._auth_header)
        error = r.status_code != GitlabPublisher.API_SUCCESS_DELETE
        if error:
            print(r.content, file=sys.stderr)
            raise_error(RuntimeError, "remove_user_from_project, status code: %s, url: %s" % (r.status_code, r.url))

class Job:
    """
    Routine à exécuter et paramètres suivant l'analyse des arguments passés à la
    ligne de commande.

    VARIABLES D'INSTANCE

    - `_job`: La fonction a exécuter.
    - `_parsed_args`: Les arguments résolus à la ligne de commande.

    """
    def __init__(self):
        self.publish_as_comment = None

    def run(self):
        if self._job:
            self._job(self._parsed_args)

    def remove_user_from_project(self, args):
        """
        Retrait d'un utilisateur de la liste des membres d'un projet.
        """
        user_to_rm = args.remove_user_from_project[0]
        pbar = progressbar.ProgressBar()
        pbar.start()
        i = 0

        gp = GitlabPublisher(args.token)
        project = gp.get_project_info(args.user, args.project)
        i += 33; pbar.update(i)
        if not project:
            raise_error(RuntimeError, "Impossible de trouver le projet.")

        users = gp.get_project_members(project["id"])
        i += 33; pbar.update(i)
        try:
            u_info = next(u for u in users if u["username"] == user_to_rm)
            gp.remove_user_from_project(project["id"], u_info["id"])
            pbar.finish()
        except StopIteration:
            pass

    def publish(self, args):
        """
        Publication d'un billet (et/ou d'un commentaire) sur un dépôt d'un
        utilisateur donné.

        VARIABLES D'INSTANCE RECQUISE

        - `publish_as_comment`: indique si le contenu devrait être publié comme
                                un commentaire au billet ou bien dans le corps
                                du billet.

        """
        pbar = progressbar.ProgressBar()
        pbar.start()
        i = 0

        text = ''.join(markfile.read(sys.stdin))
        if self.publish_as_comment:
            with open(args.issue_body_file) as f:
                itext = f.read()
                i, k = markfile.extract_title_indexes(itext)
                issue_text = itext if i == -1 else itext[k+1:]
        else:
            issue_text = text

        gp = GitlabPublisher(args.token)

        project = gp.get_project_info(args.user, args.project)
        if not project:
            raise_error(RuntimeError, "Impossible de trouver le projet.")
        i+=25; pbar.update(i)

        issue = gp.get_issue(project["id"], args.issue_title)
        note = None
        if not (issue and self.publish_as_comment):
            issue = gp.publish_issue(project["id"], args.issue_title, issue_text, iid=issue["iid"] if issue else None)
            i+=25; pbar.update(i)
        else:
            note = gp.get_issue_comment(project["id"], issue["iid"], args.comment_title)
            i+=25; pbar.update(i)

        if self.publish_as_comment:
            gp.publish_comment(project["id"], issue["iid"], text, nid=note["id"] if note else None)
        pbar.finish()

def parse_args(parser):
    job = Job()
    args = job._parsed_args = parser.parse_args()

    if args.debug:
        sys.stdin = open('/dev/tty')
        ipdb.set_trace()

    if args.version:
        print(PRG_NAME, "v"+str(VERSION))
        sys.exit(0)

    if args.remove_user_from_project:
        job._job = job.remove_user_from_project
        return job
    else:
        if args.comment_title and args.issue_body_file:
            job.publish_as_comment = True
        job._job = job.publish
        return job


def main():
    parser = argparse.ArgumentParser(
        prog=PRG_NAME,
        description="""
        Publication sur gitlab d'un fichier. Le fichier est lue en entrée standard; un nouveau billet est ouvert sur
        gitlab (s'il n'existe pas déjà); le fichier est publié directement dans la description du billet ou bien en
        commentaire.

        Si les options `-b` et `-Tc` sont fournis, alors le fichier lu sur l'entrée standard sera publié en commentaire.
        Sinon, le fichier sera publié directement dans la description du billet.

        Le programme utilise les options `-Ti` et `-Tc` afin d'identifier les billet et les commentaires respectivement.
        Les commentaires doivent donc contenir un titre afin de les identifier.
        """
    )
    parser.add_argument("-v", "--version", help="Affiche l'aide et quitte.", action="store_true")
    parser.add_argument("-d", "--debug",   help="Démarre le programme en mode DEBUG (ipdb).", action="store_true")

    publishing_args = parser.add_argument_group("publishing arguments")
    publishing_args.add_argument("-t", "--token",   help="Jeton à utiliser pour s'authentifier.", type=str, required=True)
    publishing_args.add_argument("-u", "--user",    help="Nom d'utilisateur du sujet.", type=str, required=True)
    publishing_args.add_argument("-p", "--project", help="Nom du projet.", type=str, required=True)
    publishing_args.add_argument("-b", "--issue-body-file",
            help="Le chemin vers le fichier contenant le corps du billet à créer.", type=str)
    publishing_args.add_argument("-Ti", "--issue-title", help="Titre du fichier décrivant le billet.", type=str)
    publishing_args.add_argument("-Tc", "--comment-title", help="Titre du commentaire.", type=str)

    project_management_args = parser.add_argument_group("arguments de gestion")
    project_management_args.add_argument("--remove-user-from-project", help="Retrait d'un utilisateur du projet.",
            nargs=1, type=str, metavar="USER_NAME")

    job = parse_args(parser)

    try:
        job.run()
    except Exception as e:
        traceback.print_tb(e.__traceback__)
        print(type(e).__name__+':', e, file=sys.stderr)
        sys.exit(1)

if __name__ == "__main__":
    main()

#  vim: set ts=8 sw=4 tw=120 et :

