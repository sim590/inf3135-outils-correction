# -*- coding: utf-8 -*-

import sys
import re
import datetime

from enum import Enum
from bs4 import BeautifulSoup, NavigableString

HTML_PARSER = "lxml"

def read(file_):
    """
    Lecture du fichier de correction. Séparation du début, la table et la fin de
    fichier. Seule la table devrait être lu par BeautifulSoup (HTML).
    """
    startbuf = ""
    tablebuf = ""
    endbuf   = ""
    table_start = False
    table_end = False
    for line in file_:
        table_start = True if re.match('<table>', line) else table_start
        if table_start and not table_end:
            tablebuf += line
        elif not table_start:
            startbuf += line
        else:
            endbuf += line
        table_end = True if re.match('</table>', line) else table_end
    return (startbuf, tablebuf, endbuf)

def strip_tags(html, invalid_tags):
    """Retire les tags indésirables

    @param html: Une chaîne de caractères HTML.
    @type  html: str

    @param invalid_tags: Les tags indésirables.
    @type  invalid_tags: list

    @return: La soupe.
    @rtype : BeautifulSoup
    """
    soup = BeautifulSoup(html, HTML_PARSER)
    for tag in soup.findAll(True):
        if tag.name in invalid_tags:
            tag.replaceWithChildren()
    return soup

class Parser:
    """
    Analyse le fichier de correction afin d'y retrouver les notes. Celui-ci
    contient une table au format HTML de la forme suivante:

    <table>
        <tr>
            <th>Critère</th>
            <th>Commentaire</th>
            <th>Points</th>
        </tr>
        <tr>
            <td>Section 1</td>
            <td>Justifications</td>
            <td>XX/YY</td>
        </tr>
        <tr>
            <td>Section 2</td>
            <td>Justifications</td>
            <td>XX/YY</td>
        </tr>
        <!-- ... -->
    </table>
    """
    MARK_RE = re.compile("([0-9]+\.?[0-9]*)/([0-9][0-9]?)")

    def __init__(self, soup):
        self._soup = soup

    def _get_section_tag(self, section_re):
        """Retourne le tag BeautifulSoup de la section donnée.

        @param section_re: Expression régulière déterminant la section.
        @type  section_re: str

        @return: Le tag de la section.
        @rtype : BeautifulSoup.Tag
        """
        for k,t in enumerate(self._soup.table.find_all('tr')[1:]):
            stag = t.find('td')
            if stag and re.search(section_re, stag.text):
                return stag
        return None

    def _get_penalities_tag(self, section_re):
        t = self._get_section_tag(section_re)
        return t.find_next('td') if t else None

    def _get_marking_tag(self, section_re):
        t = self._get_penalities_tag(section_re)
        return t.find_next('td') if t else None

    def get_section_marking(self, section_re):
        """Retrouve le pointage pour la section donnée.

        @param section_re: Expression régulière déterminant la section.
        @type  section_re: str

        @return: La note et le total.
        @rtype : tuple
        """
        marktag = self._get_marking_tag(section_re)
        if not marktag:
            return (None, None)
        mark_n_total = re.search(Parser.MARK_RE, marktag.text)

        if not mark_n_total:
            return (None, None)

        mark, total = mark_n_total.groups()
        return (float(mark), int(total))

def extract_title_indexes(text):
    title_n = text.find('#')
    eol_n   = text[title_n:].find('\n')
    return (title_n, eol_n)

class Revision(Parser):
    """
    Revision du fichier de correction (correction.md).
    """
    def __init__(self, soup):
        super(Revision, self).__init__(soup)

    @staticmethod
    def revised_title(text):
        REVISED_MARKER = "# (\[RÉVISÉ.*\])?(.*)"
        date = datetime.datetime.now().date()
        t = re.search(REVISED_MARKER, text).groups()
        return "# [Révisé, date: %s] " % date + t[1]

    def revised_table(self, section_re, rev_spec):
        """
        Ajuste les notes en fonction des bonus à appliquer pour chaque
        pénalités. Le contenu de self._soup est automatiquement affecté.

        @param section_re:  Expression régulière déterminant la section.
        @type  section_re:  str

        @param rev_spec:  Les différences à appliquer. La paire clef-valeur
                          (regex, diff) définie respectivement l'expression à
                          rechercher dans la liste des justifications
                          (commentaires) et la différence de point à appliquer.
        @type  rev_spec:  dict
        """
        penalitiestag = self._get_penalities_tag(section_re)
        marktag = self._get_marking_tag(section_re)
        if not (penalitiestag and marktag):
            return (False, self._soup)

        mark, total = self.get_section_marking(section_re)
        if not (mark and total):
            return (False, self._soup)

        delta = 0
        for p in rev_spec:
            if re.search(p, penalitiestag.text):
                delta += 1
                mark += float(rev_spec[p])

        if delta > 0:
            t = self._soup.new_tag('td')
            t.insert(0, '%s/%s' % (min(mark, total), total))
            marktag.replaceWith(t)

        return (delta > 0, self._soup.table)

#  vim: set ts=8 sw=4 tw=120 et :

