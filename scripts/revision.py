#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import argparse
import ipdb
import json

from bs4 import BeautifulSoup, NavigableString

import markfile

PRG_NAME = "revision"
VERSION  = 0.1

def main():
    parser = argparse.ArgumentParser(
        prog=PRG_NAME,
        description="Révision des notes d'un travail pratique du cours INF3135."
    )
    parser.add_argument("-v", "--version", help="Affiche l'aide et quitte.", action="store_true")
    parser.add_argument("-d", "--debug",   help="Démarre le programme en mode DEBUG (ipdb).", action="store_true")

    revision_args = parser.add_argument_group("revision arguments")
    revision_args.add_argument("-r", "--revision",
            help="""Une chaîne de caractères au format JSON comportant des expressions régulières
            comme clef et un nombre comme valeur. La clef est l'expression à retrouver dans le
            fichier déterminant la pénalité à réviser et le nombre est la quantité de points à
            rajouter.
            """,
            type=str,
            required=True
    )
    revision_args.add_argument("-s", "--section",   help="La section à réviser.", type=str, required=True)
    args = parser.parse_args()

    fstartbuf, data, fendbuf = markfile.read(sys.stdin)

    if args.debug:
        sys.stdin = open('/dev/tty')
        ipdb.set_trace()

    if args.version:
        print(PRG_NAME, "v"+str(VERSION))
        sys.exit(0)

    rev_spec = {}
    if args.revision:
        rev_spec = json.loads(args.revision)

    title_n, eol_n = markfile.extract_title_indexes(fstartbuf)
    revised_title = markfile.Revision.revised_title(fstartbuf[title_n:eol_n])
    fstartbuf = fstartbuf[:title_n+1] + revised_title + fstartbuf[eol_n:]

    soup = BeautifulSoup(data, markfile.HTML_PARSER)
    mfr  = markfile.Revision(soup)
    d, t = mfr.revised_table(args.section, rev_spec)
    if d:
        soup.table.replaceWith(t)
        # retrait des tags que BeautifulSoup ajoute lui-même.
        soup = markfile.strip_tags(str(soup), ['html', 'body', 'p'])
        print(fstartbuf+str(soup)+fendbuf)
    else:
        sys.exit(1)

if __name__ == "__main__":
    main()

#  vim: set ts=8 sw=4 tw=120 et :

