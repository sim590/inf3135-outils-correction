#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import re
import argparse
import ipdb
import glob
import httplib2
import time
import traceback

# Google Sheet API
from apiclient import discovery
from googleapiclient.errors import HttpError
from oauth2client.file import Storage

import progressbar
from bs4 import BeautifulSoup, NavigableString

import markfile

PRG_NAME = "sheetspublisher"
VERSION  = 0.1

MARKING_FILE_NAME = 'correction.md'

def find_student_dir_path(student_id):
    dirs = [os.path.dirname(f) for f in glob.glob('%s/*/%s' % (os.path.abspath(os.path.curdir), MARKING_FILE_NAME))]
    for d in dirs:
        with open("%s/README.md" % d) as f:
            if student_id in f.read():
                return d
    return None

class SheetsPublisher:
    """
    Récupère les codes permanents sur la feuille de calcul Google par son l'API et publie la note de l'étudiant.
    """
    def __init__(self, sheet_id, students_id_col):
        self._sheet_id           = sheet_id
        self._students_id_col    = students_id_col
        self._service = discovery.build('sheets', 'v4',
                            http=self._get_credentials().authorize(httplib2.Http()),
                            discoveryServiceUrl=('https://sheets.googleapis.com/$discovery/rest?version=v4'))

    # Le code ci-après est tiré d'un tutoriel disponible sur la page suivante:
    # https://developers.google.com/sheets/api/quickstart/python#step_3_set_up_the_sample
    def _get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, 'sheets.googleapis.com-python-quickstart.json')
        credentials = Storage(credential_path).get()
        if not credentials or credentials.invalid:
            raise RuntimeError("Informations d'authentification indisponible. Générez le fichier d'abord.")
        return credentials

    def extract_a1_range(self, a1_range_str):
        r = re.search("^([^!]*!)?([a-zA-Z]+)([0-9]+)(?::([a-zA-Z]+)([0-9]+))?$", a1_range_str)
        return r.groups() if r else None

    def get_students_id(self, rfrom, rto):
        range_ = self._students_id_col+str(rfrom)+":"+self._students_id_col+str(rto)
        result = self._service.spreadsheets().values().get(spreadsheetId=self._sheet_id, range=range_).execute()
        return result.get('values', [])

    def publish(self, col, row, mark):
        range_ = col+str(row)
        body   = { "range" : range_, "values" : [[mark]] }
        result = self._service.spreadsheets().values().update(
                        spreadsheetId=self._sheet_id,
                        range=range_,
                        valueInputOption="RAW",
                        body=body
        ).execute()
        return result

def ranges_are_valid(ranges):
    if not all([r is not None for r in ranges]):
        print("***Erreur: une plage spécifiée ne respecte pas le standard A1")
        return False
    elif not all([(r[1] == r[3] or (r[1] and r[3] is None))
                  and r[2] == ranges[0][2]
                  and r[4] == ranges[0][4] for r in ranges]):
        print("***Erreur: toutes les plages doivent décrire un parcours dans un même colonne" \
             +" débutant d'une même ligne à une même autre ligne.",
              file=sys.stderr)
        return False
    return True

def main():
    parser = argparse.ArgumentParser(
        prog=PRG_NAME,
        description="Publication des notes de travaux dans une feuille de calcul Google."
    )
    parser.add_argument("-v", "--version", help="Affiche l'aide et quitte.", action="store_true")
    parser.add_argument("-d", "--debug",   help="Démarre le programme en mode DEBUG (ipdb).", action="store_true")
    parser.add_argument("range", nargs='+',
            help="""Plage de notes à modifier. Le format est doit respecter la notation A1. Plusieurs plages peuvent
            être spécifiés en séparant par des espaces. La longueur de chaque plage doit correspondre à une même valeur.
            """,
            type=str
    )

    req_args = parser.add_argument_group("required arguments")
    req_args.add_argument("-c", "--students-id-col", help="L'identifiant de la colonne contenant les codes permanents.",
            required=True)
    req_args.add_argument("-s", "--sections", nargs='+',
            help="""Sections à publier. Chaque nom de section correspond à une plage de notes. Par exemple: -s Makefile
            ou bien encore -s 'Git'. La chaîne doit se trouver dans la première colonne du fichier de correction de
            l'étudiant.
            """,
            type=str,
            required=True
    )
    req_args.add_argument("-i", "--sheet-id",    help="Identifiant de la feuille Google Sheet.", type=str, required=True)

    args = parser.parse_args()

    if args.debug:
        ipdb.set_trace()

    if len(args.range) is not len(args.sections):
        parser.error("Le nombre de sections doit être égal au nombre de plages (voir --help).")

    if args.version:
        print(PRG_NAME, "v"+str(VERSION))
        sys.exit(0)

    spublisher = SheetsPublisher(args.sheet_id, args.students_id_col)

    ranges_ = [spublisher.extract_a1_range(r) for r in args.range]
    if not ranges_are_valid(ranges_):
        sys.exit(1)

    pbar = progressbar.ProgressBar()
    pbar.start()
    firstrow = ranges_[0][2]
    lastrow = ranges_[0][4]
    sids = spublisher.get_students_id(firstrow, lastrow if lastrow else firstrow)
    for k,id_ in enumerate(sids):
        dpath = find_student_dir_path(id_[0])
        if not dpath:
            print("Attention: %s: impossible de trouver ce code permanent..." % id_[0], file=sys.stderr)
            continue
        with open(dpath + "/%s" % MARKING_FILE_NAME) as markf:
            fstartbuf, data, fendbuf = markfile.read(markf)
            soup = BeautifulSoup(data, markfile.HTML_PARSER)
            mfparser = markfile.Parser(soup)

            i = 0
            for s,r in zip(args.sections, ranges_):
                mark,_ = mfparser.get_section_marking(s)
                try:
                    spublisher.publish(r[1], int(r[2])+k, mark)

                    p = lambda k,i,s,r0,rn: min(int((k*s+i)/s/(rn-r0+1)*100), 100)
                    pbar.update(p(k,i,len(args.sections),int(r[2]),int(r[4] if r[4] else r[2])))
                    i += 1
                except HttpError as e:
                    traceback.print_tb(e.__traceback__)
                    print(type(e).__name__+':', e, file=sys.stderr)
                    print("Interrompu au moment de la mise à jour de la cellule %s%s." % (str(r[1]), int(r[2])+k), file=sys.stderr)
                    sys.exit(1)
                # Google est tellement généreux avec sa politique de limitation requêtes par secondes et incitant à
                # l'achat d'une license illimitée... On aime ça
                time.sleep(0.85)

if __name__ == "__main__":
    main()

#  vim: set ts=8 sw=4 tw=120 et :

